package cc.linkmob.mybluetoothlibrary.connect;

/**
 * Created by WenChao on 2015/12/26.
 */
public class Constant {
    public static final String CONNECTION_UUID ="00001101-0000-1000-8000-00805F9B34FB";
    /**
     * 开始监听
     */
    public static final int MSG_START_LISTENING =1;
    /**
     * 结束监听
     */
    public static final int MSG_FINISH_LISTENING =2;
    /**
     * 有客户端链接
     */
    public static final int MSG_GOT_A_CLINET =3;
    /**
     * 连接到服务器端
     */
    public static final int MSG_CONNECTED_TO_SERVER =4;
    /**
     * 获取的数据
     */
    public static final int MSG_GOT_DATA =5;
    /**
    * 客户端中断
    */
    public static final int MSG_BREAK_CLIENT =6;
    /**
    * 服务端中断
    */
    public static final int MSG_BREAK_SERVER =10;
    /**
     * 服务器开启失败
     */
    public static final int MSG_ERROR_SERVER_ACCEPT =7;
    /**
     * 服务器开启失败
     */
    public static final int MSG_ERROR_CLIENT_CONNECT =8;
    /**
     * 信息接收失败
     */
    public static final int MSG_ERROR_READ=9;
}
