package cc.linkmob.mybluetoothlibrary.connect;

import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

/**
 * 通讯线程
 * Created by WenChao on 2015/12/26.
 */
public class ConnectedThread extends Thread {
    private final BluetoothSocket mmSocket;
    private final InputStream mmInputStream;
    private final OutputStream mmOutputStream;
    private final Handler mHandler;


    public ConnectedThread(BluetoothSocket socket, Handler handler) {
        mmSocket = socket;
        InputStream tmpIn = null;
        OutputStream tmpOut = null;
        mHandler = handler;
        //Get the input and output streams, using temp objects because
        //member streams are final
        try {
            tmpIn = socket.getInputStream();
            tmpOut = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mmInputStream = tmpIn;
        mmOutputStream = tmpOut;
    }

//    public ConnectThread(BluetoothDevice device, BluetoothAdapter adapter, Handler mUIHandler) {
//    }

    @Override
    public void run() {
        byte[] buffer = new byte[1024];//buffer store for the stream
        int bytes;//bytes returned from read()

        //keep listening to the IinputStream until an exceptioin occurs
        while (true) {
            //Read listening to the InputStream
            try {
                bytes = mmInputStream.read(buffer);
            } catch (IOException e) {
                mHandler.sendEmptyMessage(Constant.MSG_ERROR_READ);
                e.printStackTrace();
                break;
            }

            if (bytes > 0) {
                String data = null;
                try {
                    data = new String(buffer, 0, bytes, "utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                if ("server:404".equals(data)) {
                    mHandler.sendEmptyMessage(Constant.MSG_BREAK_SERVER);
                    break;
                } else if ("client:404".equals(data)) {
                    mHandler.sendEmptyMessage(Constant.MSG_BREAK_CLIENT);
                    break;
                }
                Message message = mHandler.obtainMessage(Constant.MSG_GOT_DATA, data);//处理逻辑不强，应该将buffer 传给一个listener或者handler去处理
                mHandler.sendMessage(message);
                Log.d("GOTMSG", "message size is：" + bytes);
            }


        }
    }


    /**
     * call this from the main activity to send data to the remote device
     *
     * @param bytes
     */
    public void write(byte[] bytes) {
        try {
            mmOutputStream.write(bytes);
            mmOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * call this from the main activity to shutdown the connection
     */
    public void cancel() {
        try {
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
