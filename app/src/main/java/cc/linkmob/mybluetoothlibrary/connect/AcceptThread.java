package cc.linkmob.mybluetoothlibrary.connect;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.os.Handler;

import java.io.IOException;
import java.util.UUID;

/**
 * 服务器线程
 * Created by WenChao on 2015/12/26.
 */
public class AcceptThread extends Thread {
    private static final String NAME = "BlueToothClass";
    private static final UUID MY_UUID = UUID.fromString(Constant.CONNECTION_UUID);

    private  final BluetoothServerSocket mmServerSocket;
    private  final BluetoothAdapter mBluetoothAdapter;
    private  final Handler mHandler;
    private  ConnectedThread mConnectedThread;


    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    public AcceptThread(BluetoothAdapter adapter,Handler handler){
        //user a temporary object that is later assigned to mmServerSocket,
        //because mmServerSocket is final
        mBluetoothAdapter = adapter;
        mHandler = handler;
        BluetoothServerSocket tmp = null;
        try {
            tmp = mBluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(NAME,MY_UUID);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mmServerSocket = tmp;
    }



    @Override
    public void run() {
        BluetoothSocket socket = null;
        //keep listening until exception occurs or a socket is returned
        mHandler.sendEmptyMessage(Constant.MSG_START_LISTENING);
        while (true){
            try {

                socket= mmServerSocket.accept();//阻塞
            } catch (IOException e) {
                mHandler.sendMessage(mHandler.obtainMessage(Constant.MSG_ERROR_SERVER_ACCEPT,e));
                e.printStackTrace();
                break;
            }
            if(socket!=null){
                manageConnectedSocket(socket);
                try {
                    mmServerSocket.close();
//                    mHandler.sendEmptyMessage(Constant.MSG_FINISH_LISTENING);
                } catch (IOException e) {
                    e.printStackTrace();

                }
                break;
            }
        }
//        super.run();
    }

    private void manageConnectedSocket(BluetoothSocket socket) {
        //只支持同时处理一个链接，如果需要支持多可客户端链接，需要创建一个连接池
        if(mConnectedThread !=null){
            mConnectedThread.cancel();
        }
        mHandler.sendEmptyMessage(Constant.MSG_GOT_A_CLINET);
        mConnectedThread = new ConnectedThread(socket,mHandler);
        mConnectedThread.start();
    }

    /**
     * will cancel the listening socket,and cause the thread to finish
     */
    public void cancel(){
        try {
            sendData("server:404".getBytes());
            mmServerSocket.close();
            mHandler.sendEmptyMessage(Constant.MSG_FINISH_LISTENING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendData(byte[] data){
        if(mConnectedThread !=null){
            mConnectedThread.write(data);
        }
    }
}
