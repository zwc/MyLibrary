package cc.linkmob.mybluetoothlibrary.connect;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Build;
import android.os.Handler;

import java.io.IOException;
import java.util.UUID;

/**
 * 客户端线程
 * Created by WenChao on 2015/12/26.
 */
public class ConnectThread extends Thread {
    private final Handler mHandler;
    private static final UUID MY_UUID = UUID.fromString(Constant.CONNECTION_UUID);

    private final BluetoothSocket mmSocket;
    private final BluetoothDevice mmDevice;
    private final BluetoothAdapter mBluetoothAdapter;
    private ConnectedThread mConnectedThread;

    @TargetApi(Build.VERSION_CODES.GINGERBREAD_MR1)
    public ConnectThread(BluetoothDevice device, BluetoothAdapter adapter, Handler handler) {
        //use a temporaty object that is later assigned to mmsocket.
        //because mmsocket is final
        BluetoothSocket tmp = null;
        mmDevice = device;
        mBluetoothAdapter = adapter;
        mHandler = handler;
        //Get a bluetoothSocket to connect with the given BluetoothDevice
        try {
            tmp = device.createInsecureRfcommSocketToServiceRecord(MY_UUID);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mmSocket = tmp;

    }

    @Override
    public void run() {
        //cancel discovery because it will slow down the conncetion
        mBluetoothAdapter.cancelDiscovery();
        //connect the device through the sockst.this will block
        //until it succeed or throws an exception
        try {
            mmSocket.connect();//阻塞函数
        } catch (IOException e) {
            mHandler.sendMessage(mHandler.obtainMessage(Constant.MSG_ERROR_CLIENT_CONNECT, e));
            //unable to connect;close the socket and get out
            e.printStackTrace();
            try {
                mmSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }
        //de work to manage the connection (in a separete thread)
        manageConnectedSocket(mmSocket);
    }

    private void manageConnectedSocket(BluetoothSocket mmSocket) {
        mHandler.sendEmptyMessage(Constant.MSG_CONNECTED_TO_SERVER);
        mConnectedThread = new ConnectedThread(mmSocket,mHandler);
        mConnectedThread.start();
    }

    /**
     * will cancel an in-progress connection, and close the socket
     */
    public void cancel() {
        try {
            sendData("client:404".getBytes());
            mmSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public  void  sendData(byte[] data){
        if(mConnectedThread!=null ){
            mConnectedThread.write(data);
        }
    }
}
