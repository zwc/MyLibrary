package cc.linkmob.mybluetoothlibrary.controller;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;

import java.util.ArrayList;
import java.util.List;


/**
 * 蓝牙适配器
 * Created by WenChao on 2015/12/25.
 */
public class BlueToothController {

    /**
     * 搜索ble设备时，为了省电，要及时停止搜素。这里每次搜索的时间，默认10s
     */
    private static final int SCAN_PERIOD = 10000;


    /**
     * 是否在进行ble搜索
     */
    private boolean isScanning = false;

    private Handler mHandler = new Handler();

//    private  Context mContext;

    private BluetoothLeScanner mScanner;

    public BluetoothAdapter getmAdapter() {
        return mAdapter;
    }

    private BluetoothAdapter mAdapter;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BlueToothController(Context context) {
//        mContext = context;
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mAdapter = bluetoothManager.getAdapter();
        mScanner = mAdapter.getBluetoothLeScanner();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public BlueToothController() {
        mAdapter = BluetoothAdapter.getDefaultAdapter();
        mScanner = mAdapter.getBluetoothLeScanner();
    }

    /**
     * 是否支持蓝牙
     *
     * @return true 支持，false 不支持
     */
    public boolean isSupportBlueTooth() {
        if (mAdapter != null) {
            return true;
        } else {

            return false;
        }

    }

//    /**
//     * 是否支持BLE
//     *
//     * @return true 支持，false 不支持
//     */
//    public boolean isSupportBlueToothLE(Context actviity) {
//
//        return actviity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
//    }

    /**、
//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void scanLeDevice(final ScanCallback leScanCallback) {
////        if (!isScanning) {
////            // Stops scanning after a pre-defined scan period.
////            mHandler.postDelayed(new Runnable() {
////                @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
////                @Override
////                public void run() {
////                    isScanning = false;
////                    mAdapter.stopLeScan(leScanCallback);
////                }
////            }, SCAN_PERIOD);
////
////            isScanning = true;
////            mAdapter.startLeScan(leScanCallback);
////        } else {
////            isScanning = false;
////            mAdapter.stopLeScan(leScanCallback);
////        }
//
//        if (!isScanning) {
//            // Stops scanning after a pre-defined scan period.
//            mHandler.postDelayed(new Runnable() {
//                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//                @Override
//                public void run() {
//                    isScanning = false;
//                    mScanner.stopScan(leScanCallback);
//                }
//            }, SCAN_PERIOD);
//
//            isScanning = true;
//            mScanner.startScan(leScanCallback);
//        } else {
//            isScanning = false;
//            mScanner.stopScan(leScanCallback);
//        }
//    }

    /**
     * 打卡蓝牙的可见性
     *
     * @param context
     */
    public void enableVisibly(Context context) {
        Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300); //300s
        context.startActivity(discoverableIntent);

    }

    /**
     * 查找设备
     */
    public void findDevice() {
        assert (mAdapter != null);
        mAdapter.startDiscovery();
    }

    /**
     * 获取绑定设备
     *
     * @return
     */
    public List<BluetoothDevice> getBondedDeviceLsit() {
        return new ArrayList<>(mAdapter.getBondedDevices());
    }

    /**
     * 判断当前蓝牙状态
     *
     * @return
     */
    public boolean getBlueToothStatus() {
        assert (mAdapter != null);

        return mAdapter.isEnabled();
//        return mAdapter.isDiscovering();
    }

    /**
     * 打开蓝牙
     *
     * @param activtiy
     * @param requestCode
     */
    public void turnOnBlueTooth(Activity activtiy, int requestCode) {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        activtiy.startActivityForResult(intent, requestCode);
//        mAdapter.enable();
    }

    /**
     * 关闭蓝牙
     */
    public void turnOffBlueTooth() {
        mAdapter.disable();
    }

}
